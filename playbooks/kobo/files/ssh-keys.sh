#!/bin/sh

set -e

set -- 'rsa' 'dss' 'ecdsa' 'ed25519'

for algo in "$@";
do
    key="/etc/dropbear/dropbear_${algo}_host_key"
    if [ ! -f "${key}" ];
    then
        /usr/local/bin/dropbearmulti dropbearkey -t "${algo}" -f "${key}"
    fi
done
