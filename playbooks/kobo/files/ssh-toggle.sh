#!/bin/sh

set -e

cd "$(dirname "${0}")"
echo "Working directory: ${PWD}"


if /bin/mount -t devpts | /bin/grep -q /dev/pts; then
    echo "devpts already mounted"
else
    echo "Mounting devpts"
    /bin/mkdir -p /dev/pts && /bin/mount -t devpts devpts /dev/pts;
fi

if [ -f /var/run/dropbear.pid ]; then
    echo "Killing ssh server"
    kill "$(cat /var/run/dropbear.pid)" 2>&1
else
    echo "Staring ssh server at $(/sbin/ifconfig | /usr/bin/awk '/inet addr/{print substr($2,6)}')"
    /usr/local/bin/dropbearmulti dropbear -E \
        -e -s -T 3 -j -k -K 60 \
        "${@}" 2>&1
fi
